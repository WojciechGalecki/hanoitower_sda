package HanoiTower;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Game {

    private Map<Integer, Stack<Integer>> mapStack = new HashMap<>();

    public Game(Integer size) {
        mapStack.put(1, new Stack<>());
        mapStack.put(2, new Stack<>());
        mapStack.put(3, new Stack<>());
        for (int i = size; i >= 1; i--) {
            mapStack.get(1).push(i);
        }
    }

    public void isMove(Integer from, Integer to) {
        try {
            if (mapStack.get(to).empty()) moveElement(from,to);
            else if(mapStack.get(from).peek() > mapStack.get(to).peek()) {
                System.err.println("Invalid operation!");
            }
            else moveElement(from,to);
        } catch (NullPointerException nPe) {
            System.err.println("Wrong number disc!");
        } catch (EmptyStackException eSe) {
            eSe.printStackTrace();
        }

    }

    public void moveElement(Integer from, Integer to) {
            Integer temp = mapStack.get(from).pop();
            mapStack.get(to).push(temp);
    }

    public void print() {
        for (Stack value : mapStack.values()) {
            System.out.println(value);
        }
    }

    public boolean endGame() {
        if (mapStack.get(1).isEmpty() && mapStack.get(2).isEmpty()) return true;
        else return false;
    }
}


