package HanoiTower;

import java.util.Scanner;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("new [new game] quit [exit]");
        while(input.hasNextLine()){
            String command = input.nextLine();
            switch (command) {
                case "quit":
                    return;
                case "new": {
                    System.out.println("<Hanoi Tower Game>  How many discs?");
                    Integer size = input.nextInt();
                    Game g1 = new Game(size);
                    do {
                        g1.print();
                        System.out.print("Your move from: ");
                        Integer from = input.nextInt();
                        System.out.print("Your move to: ");
                        Integer to = input.nextInt();
                        g1.isMove(from,to);
                    } while (g1.endGame() == false);
                    g1.print();
                    System.out.println("Congratulations, You win!!!");
                    return;                   // break; wyrzuca wrong commant w glownym menu???
                }
                default: {
                    System.err.println("Wrong command!!!");
                    continue;
                }
            }
        }
    }
}
